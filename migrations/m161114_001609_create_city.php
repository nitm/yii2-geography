<?php

use yii\db\Migration;

class m161114_001609_create_city extends Migration
{
    public function safeUp()
    {
        $this->createTable('nitm_geography_city', [
                'id' => $this->primaryKey(),
                'name' => $this->string(128)->notNull(),
                'zipcode' => $this->string(16),
                'latitude' => $this->decimal(10, 6),
                'longitude' => $this->decimal(10, 6),
                'state_id' => $this->integer(),
                'country_id' => $this->integer(),
                'enabled' => $this->boolean()
           ]);

           //Only unique addresss
            $this->createIndex(
                'city_codes',
                'nitm_geography_city',
                ['zipcode']
            );

            // add foreign key for table `nitm_geography_state`
            $this->addForeignKey(
                'nitm_geography_city_state-fk',
                'nitm_geography_city',
                'state_id',
                'nitm_geography_state',
                'id',
                'CASCADE'
            );

           // add foreign key for table `nitm_geography_country`
           $this->addForeignKey(
               'nitm_geography_city_country-fk',
               'nitm_geography_city',
               'country_id',
               'nitm_geography_country',
               'id',
               'CASCADE'
           );

           //Insert the cities data
           $this->db->createCommand(file_get_contents(__DIR__.'/../fixtures/cities.sql'))->execute();
    }

    public function safeDown()
    {
        echo "m161114_001609_create_city cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
