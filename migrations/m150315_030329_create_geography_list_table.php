<?php

use yii\db\Schema;
use yii\db\Migration;

class m150315_030329_create_geography_list_table extends Migration
{
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_geography_list');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_geography_list', [
            'id' => 'pk',
            'type' => Schema::TYPE_STRING.'(32) NOT NULL',
            'author_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'created_at' => Schema::TYPE_TIMESTAMP.' NOT NULL DEFAULT NOW()',
            'editor_id' => Schema::TYPE_INTEGER.' NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP.' NULL',
            'parent_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'parent_type' => Schema::TYPE_STRING.'(32) NOT NULL',
            'parent_class' => Schema::TYPE_TEXT.' NOT NULL',
            'remote_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'remote_type' => Schema::TYPE_STRING.'(32) NOT NULL',
            'remote_class' => Schema::TYPE_TEXT.' NOT NULL',
            'priority' => Schema::TYPE_INTEGER.' NULL',
            'deleted_at' => Schema::TYPE_TIMESTAMP.' NULL',
            'deleted_by' => Schema::TYPE_INTEGER.' NULL',
            'disabled_at' => Schema::TYPE_TIMESTAMP.' NULL',
            'disabled_by' => Schema::TYPE_INTEGER.' NULL',
        ]);

        $this->createIndex('nitm_geography_list_parent_id', '{{%nitm_geography_list}}', ['parent_id']);
        $this->createIndex('nitm_geography_list_remote_id', '{{%nitm_geography_list}}', ['remote_id']);
        $this->createIndex('nitm_geography_list_unique', '{{%nitm_geography_list}}', [
            'remote_id', 'remote_type', 'parent_id', 'parent_type'
        ], true);

        $this->addForeignKey('fk_nitm_geography_list_author', '{{%nitm_geography_list}}', 'author_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_nitm_geography_list_editor', '{{%nitm_geography_list}}', 'editor_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');

        /**
         * Create the metadata table.
         */
        $this->createTable('nitm_geography_list_metadata', [
            'id' => 'pk',
            'content_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'key' => Schema::TYPE_STRING.'(32) NOT NULL',
            'value' => Schema::TYPE_TEXT.' NOT NULL',
            'created_at' => Schema::TYPE_TIMESTAMP.' NOT NULL DEFAULT NOW()',
            'updated_at' => Schema::TYPE_TIMESTAMP.' NULL',
        ]);

        $this->addForeignKey('fk_nitm_geography_list_metadata', '{{%nitm_geography_list_metadata}}', 'content_id', '{{%nitm_geography_list}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function safeDown()
    {
        echo "m150315_030329_create_nitm_geography_list_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
