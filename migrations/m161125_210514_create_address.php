<?php

use yii\db\Migration;
use yii\db\Schema;

class m161125_210514_create_address extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
 public function safeUp()
 {
     $tableSchema = \Yii::$app->db->getTableSchema('nitm_geography_address');
     if ($tableSchema) {
         return true;
     }
     $this->createTable('nitm_geography_address', [
          'id' => 'pk',
          'title' => $this->string(128),
          'address' => $this->text(),
          'latitude' => $this->decimal(10, 6)->notNull(),
          'longitude' => $this->decimal(10, 6)->notNull(),
          'city_id' => $this->integer()->notNull(),
          'state_id' => $this->integer()->notNull(),
          'country_id' => $this->integer()->notNull(),
          'neighborhood_id' => $this->integer()->notNull(),
          'created_at' => $this->timestamp()->defaultExpression('NOW()'),
          'deleted_at' => $this->timestamp(),
     ]);
     $this->addForeignKey('fk_nitm_geography_address_neighborhood', '{{%nitm_geography_address}}', 'neighborhood_id', '{{%nitm_geography_neighborhood}}', 'id', 'CASCADE', 'RESTRICT');
     $this->addForeignKey('fk_nitm_geography_address_city', '{{%nitm_geography_address}}', 'city_id', '{{%nitm_geography_city}}', 'id', 'CASCADE', 'RESTRICT');
     $this->addForeignKey('fk_nitm_geography_address_state', '{{%nitm_geography_address}}', 'state_id', '{{%nitm_geography_state}}', 'id', 'CASCADE', 'RESTRICT');
     $this->addForeignKey('fk_nitm_geography_address_country', '{{%nitm_geography_address}}', 'country_id', '{{%nitm_geography_country}}', 'id', 'CASCADE', 'RESTRICT');

     /**
      * Create the metadata table.
      */
     $this->createTable('nitm_geography_address_metadata', [
          'id' => 'pk',
          'content_id' => Schema::TYPE_INTEGER.' NOT NULL',
          'key' => Schema::TYPE_STRING.'(32) NOT NULL',
          'value' => Schema::TYPE_TEXT.' NOT NULL',
          'created_at' => Schema::TYPE_TIMESTAMP.' NOT NULL DEFAULT NOW()',
          'updated_at' => Schema::TYPE_TIMESTAMP.' NULL',
     ]);

     $this->addForeignKey('fk_nitm_geography_address_metadata', '{{%nitm_geography_address_metadata}}', 'content_id', '{{%nitm_geography_address}}', 'id', 'CASCADE', 'RESTRICT');
 }

    public function safeDown()
    {
        echo "m161125_210101_create_address cannot be reverted.\n";

        return false;
    }
}
