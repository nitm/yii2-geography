<?php

use yii\db\Migration;

class m161114_001614_create_neighborhood extends Migration
{
    public function safeUp()
    {
        $this->createTable('nitm_geography_neighborhood', [
                  'id' => $this->primaryKey(),
                  'name' => $this->string(4)->notNull(),
                  'zipcode' => $this->string(16),
                  'latitude' => $this->decimal(10, 6),
                  'longitude' => $this->decimal(10, 6),
                  'city_id' => $this->integer(),
                  'state_id' => $this->integer(),
                  'country_id' => $this->integer(),
                  'enabled' => $this->boolean()
            ]);

              //Only unique addresss
               $this->createIndex(
                   'neighborhood_coords',
                   'nitm_geography_neighborhood',
                   ['latitude', 'longitude'],
                   true
               );

              // add foreign key for table `nitm_geography_state`
              $this->addForeignKey(
                  'nitm_geography_neighbohood_city-fk',
                  'nitm_geography_neighborhood',
                  'city_id',
                  'nitm_geography_city',
                  'id',
                  'CASCADE'
              );

             // add foreign key for table `nitm_geography_state`
             $this->addForeignKey(
                 'nitm_geography_neighborhood_state-fk',
                 'nitm_geography_neighborhood',
                 'state_id',
                 'nitm_geography_state',
                 'id',
                 'CASCADE'
             );

            // add foreign key for table `nitm_geography_country`
            $this->addForeignKey(
                 'nitm_geography_neighborhood_country-fk',
                 'nitm_geography_neighborhood',
                 'country_id',
                 'nitm_geography_country',
                 'id',
                 'CASCADE'
            );
    }

    public function safeDown()
    {
        echo "m161114_001614_create_neighborhood cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
