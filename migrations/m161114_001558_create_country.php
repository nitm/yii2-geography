<?php

use yii\db\Migration;

class m161114_001558_create_country extends Migration
{
    public function safeUp()
    {
        $this->createTable('nitm_geography_country', [
            'id' => $this->primaryKey(),
            'code' => $this->string(4)->notNull(),
            'clean_name' => $this->string(255),
            'name' => $this->string(255),
            'iso3_code' => $this->string(4),
            'numcode' => $this->integer(4),
            'phonecode' => $this->integer(5),
            'enabled' => $this->boolean()
       ]);

       //Only unique addresss
        $this->createIndex(
            'country_codes',
            'nitm_geography_country',
            ['code', 'iso3_code'],
            true
        );

        //Insert the country data
        $this->db->createCommand(file_get_contents(__DIR__.'/../fixtures/country.sql'))->execute();
    }

    public function safeDown()
    {
        echo "m161114_001558_create_country cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
