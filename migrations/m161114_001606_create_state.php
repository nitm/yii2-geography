<?php

use yii\db\Migration;

class m161114_001606_create_state extends Migration
{
    public function safeUp()
    {
        $this->createTable('nitm_geography_state', [
              'id' => $this->primaryKey(),
              'name' => $this->string(255)->notNull(),
              'code' => $this->string(4),
              'country_id' => $this->integer(),
              'enabled' => $this->boolean()
         ]);

         //Only unique addresss
          $this->createIndex(
              'state_codes',
              'nitm_geography_state',
              ['code'],
              true
          );

          // add foreign key for table `user`
          $this->addForeignKey(
              'nitm_geography_state_country-fk',
              'nitm_geography_state',
              'country_id',
              'nitm_geography_country',
              'id',
              'CASCADE'
          );

          //Insert the state data
          $this->db->createCommand(file_get_contents(__DIR__.'/../fixtures/states.sql'))->execute();
    }

    public function safeDown()
    {
        echo "m161114_001606_create_state cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
