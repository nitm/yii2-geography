<?php

use yii\db\Migration;
use yii\db\Schema;

class m161125_210614_create_address_list extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_geography_address_list');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_geography_address_list', [
            'id' => 'pk',
            'author_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'created_at' => Schema::TYPE_TIMESTAMP.' NOT NULL DEFAULT NOW()',
            'editor_id' => Schema::TYPE_INTEGER.' NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP.' NULL',
            'remote_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'remote_type' => Schema::TYPE_STRING.'(32) NOT NULL',
            'remote_class' => Schema::TYPE_TEXT.' NOT NULL',
            'address_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'priority' => Schema::TYPE_INTEGER.' NULL',
            'deleted' => Schema::TYPE_BOOLEAN.' NULL DEFAULT false',
            'is_primary' => Schema::TYPE_BOOLEAN.' NULL DEFAULT false',
            'deleted_at' => Schema::TYPE_TIMESTAMP.' NULL',
            'deleted_by' => Schema::TYPE_INTEGER.' NULL',
            'disabled' => Schema::TYPE_BOOLEAN.' NULL DEFAULT false',
            'disabled_at' => Schema::TYPE_TIMESTAMP.' NULL',
            'disabled_by' => Schema::TYPE_INTEGER.' NULL',
        ]);

        $this->createIndex('nitm_geography_address_list_address_id', '{{%nitm_geography_address_list}}', ['address_id']);
        $this->createIndex('nitm_geography_address_list_remote_id', '{{%nitm_geography_address_list}}', ['remote_id']);
        $this->createIndex('nitm_geography_address_list_unique', '{{%nitm_geography_address_list}}', [
            'remote_id', 'remote_type', 'address_id',
        ], true);

        $this->addForeignKey('fk_nitm_geography_address_list_author', '{{%nitm_geography_address_list}}', 'author_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_nitm_geography_address_list_editor', '{{%nitm_geography_address_list}}', 'editor_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_nitm_geography_address_list_address', '{{%nitm_geography_address_list}}', 'address_id', '{{%nitm_geography_address}}', 'id', 'CASCADE', 'RESTRICT');

        /**
         * Create the metadata table.
         */
        $this->createTable('nitm_geography_address_list_metadata', [
            'id' => 'pk',
            'content_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'key' => Schema::TYPE_STRING.'(32) NOT NULL',
            'value' => Schema::TYPE_TEXT.' NOT NULL',
            'created_at' => Schema::TYPE_TIMESTAMP.' NOT NULL DEFAULT NOW()',
            'updated_at' => Schema::TYPE_TIMESTAMP.' NULL',
        ]);

        $this->addForeignKey('fk_nitm_geography_address_list_metadata', '{{%nitm_geography_address_list_metadata}}', 'content_id', '{{%nitm_geography_address_list}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function safeDown()
    {
        echo "m161125_210101_create_address_list cannot be reverted.\n";

        return false;
    }
}
