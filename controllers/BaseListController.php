<?php

namespace nitm\geography\controllers;

use nitm\helpers\Response;
use nitm\helpers\ArrayHelper;
use nitm\controllers\DefaultApiController;

/**
 * BaseListController implements useful functions for handling lists.
 */
abstract class BaseListController extends DefaultApiController
{
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'set-priority',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbFilter' => [
                'actions' => [
                    'set-priority' => ['post'],
                    'index' => ['post', 'get'],
                ],
            ],
        ];

        return array_replace_recursive(parent::behaviors(), $behaviors);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $actions = parent::actions();

        return array_merge($actions, [
            'set-priority' => [
                'class' => \nitm\actions\PriorityAction::className(),
            ],
            'disable' => [
                'class' => \nitm\actions\PriorityAction::className(),
            ],
        ]);
    }

    public function actionIndex($type = null, $id = null, $options = [])
    {
        $widgetClass = ArrayHelper::remove($options, 'widgetClass');
        if (!class_exists($widgetClass)) {
            throw new \yii\web\NotFoundHttpException("Unable to find list for $type");
        }
        if ($this->responseFormat == 'json' || $this->responseFormat == 'xml') {
            $ret_val = $this->model->find()->where($this->getIndexFindWhere($type, $id))->all();
        } else {
            $ret_val = null;
            if (!$this->isResponseFormatSpecified) {
                $this->setResponseFormat('widget');
            }
            if ($this->responseFormat == 'widget') {
                Response::viewOptions('args', [
                    'widgetClass' => $widgetClass,
                    'options' => $options,
                ]);
            } else {
                Response::viewOptions('args.content', $widgetClass::widget($options));
            }
        }

        return $this->renderResponse($ret_val, Response::viewOptions(), \Yii::$app->request->isAjax);
    }

     /**
      * Creates a new List model.
      *
      * @return mixed
      */
     public function actionCreate($modelClass = null)
     {
         $modelClass = $modelClass ?: $this->model->className();

         return parent::actionCreate($modelClass);
     }

    /**
     * Creates a new List model.
     *
     * @return mixed
     */
    public function actionUpdate($type, $id = null, $item = null, $modelClass = null)
    {
        $modelClass = $modelClass ?: $this->model->className();

        return parent::actionUpdate($this->getId([$type, $id, $item]), $modelClass);
    }

    /**
     * Displays a single BasicIngredientList model.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionView($type, $id = null, $item = null, $modelClass = null)
    {
        $modelClass = $modelClass ?: $this->model->className();

        return parent::actionView($this->getId([$type, $id, $item]), $modelClass);
    }

    /**
     * Deletes an existing BasicIngredientList model.
     * If deletion is successful return Json result.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionDelete($type, $id = null, $item = null, $modelClass = null)
    {
        $modelClass = $modelClass ?: $this->model->className();

        return parent::actionDelete($this->getId([$type, $id, $item]), $modelClass);
    }
}
