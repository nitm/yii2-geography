<?php

namespace nitm\geography\controllers;

use nitm\geography\models\Country;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class CountryController extends DefaultController
{
    public function init()
    {
        $this->model = new Country();
    }

    public function behaviors()
    {
        $behaviors = [
        ];

        return array_merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all Country models.
     *
     * @return mixed
     */
    public function actionIndex($className = null, $options = [])
    {
        return parent::actionIndex(Country::className(), [
            'defaultParams' => [$this->model->formName() => ['disabled' => false]],
        ]);
    }
}
