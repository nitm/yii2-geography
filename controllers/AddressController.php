<?php

namespace nitm\geography\controllers;

use nitm\geography\models\Address;

/**
 * AddressController implements the CRUD actions for Address model.
 */
class AddressController extends DefaultController
{
    public function init()
    {
        parent::init();
        $this->model = new Address(['scenario' => 'default']);
    }

    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), [
            'access' => [
                'rules' => [
                    [
                        'actions' => [
                            'save-address',
                            'save-contact',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'actions' => [
                    'save-address' => ['post'],
                    'save-contact' => ['post'],
                ],
            ],
        ]);
    }

    public function actionSaveAddress($id = null)
    {
        if ($id) {
            return parent::actionUpdate($id, Address::className());
        } else {
            return parent::actionCreate(Address::className());
        }
    }

    public function actionSaveContact($id = null)
    {
        if ($id) {
            return parent::actionUpdate($id, Contact::className());
        } else {
            return parent::actionCreate(Contact::className());
        }
    }

    public function getWith()
    {
        return array_merge(parent::getWith(), [
            'address', 'contactInfo', 'drinkMeta', 'ambianceMeta',
        ]);
    }
}
