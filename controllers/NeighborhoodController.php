<?php

namespace nitm\geography\controllers;

use nitm\geography\models\Neighborhood;

/**
 * NeighborhoodController implements the CRUD actions for Neighborhood model.
 */
class NeighborhoodController extends DefaultController
{
    public $legend = [
        'success' => 'Active',
        'disabled' => 'Inactive',
    ];

    public function init()
    {
        parent::init();
        $this->model = new Neighborhood(['scenario' => 'default']);
    }

    /**
     * Lists all City models.
     *
     * @return mixed
     */
    public function actionIndex($className = null, $options = [])
    {
        return parent::actionIndex(Neighborhood::className(), [
            'with' => [
                'state', 'country', 'city',
            ],
        ]);
    }
}
