<?php

namespace nitm\geography\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use nitm\helpers\Response;

/**
 * StateController implements the CRUD actions for State model.
 */
class ListController extends \nitm\controllers\DefaultApiController
{
	public $legend = [
		'success' => 'Active',
		'disabled' => 'Inactive',
	];

	public function init()
	{
		parent::init();
	}

    /**
     * Lists all City models.
     * @return mixed
     */
    public function actionIndex($type)
    {
		$ret_val = [];
		switch($type)
		{
			case 'state':
			case 'country':
			case 'city':
			case 'neighborhood':
			$className = '\\nitm\\geography\\models\\search\\'.ucfirst($type);
			$ret_val = $this->getList($className, [
				'construct' => [
					'booleanSearch' => true,
					'inclusiveSearch' => false,
					'exclusiveSearch' => true,
					'expand' => 'none'
				]
			], false);
			break;
		}
		if(!$this->isResponseFormatSpecified)
			$this->setResponseFormat('json');
		return $this->renderResponse($ret_val);
    }
}
