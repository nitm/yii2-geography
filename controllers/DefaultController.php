<?php

namespace nitm\geography\controllers;

use nitm\geography\models\Country;
use nitm\helpers\ArrayHelper;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class DefaultController extends \nitm\controllers\DefaultController
{
    /**
    * Performs filtering on models. Custom filter that creates a model if it can be proeprly validated.
    *
    * @method actionFilter
    *
    * @param array $options      Options for the view parameters
    * @param array $modelOptions Options for the search model
    *
    * @return string Rendered data
    */
   public function afterAction($action, $result)
   {
       if (\Yii::$app->request->isAjax) {
           if (!$result['success']) {
               if ($this->trySaving()) {
                   $result['data'] = [
                     $this->model->toArray(),
                  ];
                   $result['success'] = true;
                   $result['message'] = 'Found 1 result matching your filter';
               }
           }
       }
       $result = parent::afterAction($action, $result);

       return $result;
   }

   /**
    * This will save a model that is filtered using $_GET parameters.
    *
    * @return [type] [description]
    */
   protected function trySaving()
   {
       switch ($this->model->className()) {
         case \nitm\geography\models\City::className():
         case \nitm\geography\models\State::className():
         case \nitm\geography\models\Neighborhood::className():
         $params = array_merge($_GET, ArrayHelper::getValue($_GET, '_save', []));
          $this->model->load($params, '');
          if ($this->model->validate()) {
              return $this->model->save();
          } else {
              return false;
          }
         break;
      }

       return false;
   }
}
