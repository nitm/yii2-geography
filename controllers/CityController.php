<?php

namespace nitm\geography\controllers;

use nitm\geography\models\City;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends DefaultController
{
    public $legend = [
        'success' => 'Active',
        'disabled' => 'Inactive',
    ];

    public function init()
    {
        parent::init();
        $this->model = new City(['scenario' => 'default']);
    }

    public function behaviors()
    {
        $behaviors = [
        ];

        return array_merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all City models.
     *
     * @return mixed
     */
    public function actionIndex($className = null, $options = [])
    {
        return parent::actionIndex(City::className(), [
            'with' => [
                'state', 'country',
            ],
        ]);
    }
}
