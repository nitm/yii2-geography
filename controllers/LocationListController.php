<?php

namespace nitm\geography\controllers;

use nitm\geography\models\LocationList;

/**
 * InstructionController implements the CRUD actions for AddressList model.
 */
class LocationListController extends BaseListController
{
    public function init()
    {
        parent::init();
        $this->model = new LocationList(['scenario' => 'default']);
    }

    public function behaviors()
    {
        $behaviors = [
        ];

        return array_merge(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all AddressList models.
     *
     * @return mixed
     */
    public function actionIndex($type = null, $id = null, $options = [])
    {
        $className = $this->model->properClassName($type);

        return parent::actionIndex($type, $id, [
            'widgetClass' => \nitm\geography\widgets\LocationList::className(),
            'model' => $className::findOne($id),
            'listModel' => new LocationList([
                'remote_type' => $type,
                'remote_id' => $id,
            ]),
        ]);
    }
}
