<?php

namespace nitm\geography\controllers;

use nitm\geography\models\State;

/**
 * StateController implements the CRUD actions for State model.
 */
class StateController extends DefaultController
{
    public $legend = [
        'success' => 'Active',
        'disabled' => 'Inactive',
    ];

    public function init()
    {
        parent::init();
        $this->model = new State(['scenario' => 'default']);
    }

    /**
     * Lists all City models.
     *
     * @return mixed
     */
    public function actionIndex($className = null, $options = [])
    {
        return parent::actionIndex(State::className(), [
            'with' => [
                'country',
            ],
        ]);
    }
}
