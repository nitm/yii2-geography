<?php

namespace nitm\geography;

/**
 * Module class.
 */
class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    public $googleOptions;

    public function init()
    {
        parent::init();

        // custom initialization code goes here

        /**
         * Aliases for nitm\widgets module.
         */
        \Yii::setAlias('nitm/geography', dirname(__DIR__).'/yii2-geography');
    }
    public static function getUrls($id = 'nitm-geography')
    {
        $parameters = [];
        $routeHelper = new \nitm\helpers\Routes([
            'moduleId' => $id,
            'map' => [
                'type-id-key' => '<controller:%controllers%>/<action>/<type>/<id:\d+>/<key>',
                'type-id' => '<controller:%controllers%>/<action>/<type>/<id:\d+>',
                'type' => '<controller:%controllers%>/<action>/<type>',
                'id' => '<controller:%controllers%>/<action>/<id:\d+>',
                'action-only' => '<controller:%controllers%>/<action>',
                'none' => '<controller:%controllers%>',
            ],
            'controllers' => [
                'country', 'state', 'neighborhood', 'city', 'location-list', 'address',
            ],
        ]);
        $parameters = array_keys($routeHelper->map);
        $routeHelper->map['type-index'] = ['<controller:%controllers%>/<type>' => '<controller>/index'];
        $parameters['type-index'] = ['list'];
        $routes = $routeHelper->create($parameters);

        return $routes;
    }

    public function bootstrap($app)
    {
        /**
         * Setup urls.
         */
        $app->getUrlManager()->addRules($this->getUrls(), false);
    }
}
