<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, NitmGeography.com, 2014
 *
 * @version 1.0.0
 */

namespace nitm\geography\widgets;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@nitm\geography.com>
 *
 * @since 1.0
 */
class LocationList extends BaseList
{
    public $withForm = true;
    public $listClass = 'nitm\geography\models\LocationList';
    public $listView = '@nitm/geography/views/location-list/index';
    public $formWidgetClass = '\nitm\geography\widgets\LocationListForm';

    protected $arrayDataProvider = 'locations';
    protected $assetClass = '\nitm\geography\widgets\assets\LocationListAsset';
}
