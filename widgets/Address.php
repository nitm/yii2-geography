<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, NitmGeography.com, 2014
 *
 * @version 1.0.0
 */

namespace nitm\geography\widgets;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@nitm\geography.com>
 *
 * @since 1.0
 */
class Address extends \yii\base\widget
{
    public $key;
    public $libraries = ['places'];
    public $type;
    public $model;
    public $actionUrl = 'nitm/geography/address/create';

    public $options = [
        'class' => 'well col-md-12 col-lg-12 clearfix',
    ];

    private $canRender = true;

    public function init()
    {
        if (!$this->model instanceof \nitm\geography\LocationList) {
            throw new \yii\base\InvalidConfigException("The model should be an instance of nitm\geography\LocationList");
        }
        $asset = new assets\AddressAsset();
        \nitm\geography\widgets\assets\GoogleMapsAsset::register($this->view);
        $asset->register($this->getView());
    }

    public function run()
    {
        if ($this->model->id) {
            $this->actionUrl .= '/'.$this->model->id;
        }
        $options = [
            'title' => $this->model->address()->title(),
            'scenario' => 'create',
            'param' => 'create',
            'view' => '@nitm/geography/views/address/address',
            'args' => [],
            'modelClass' => $this->model->className(),
            'formOptions' => [
                'action' => \Yii::$app->urlManager->createUrl([$this->actionUrl, '_format' => 'json']),
            ],
        ];
        $options = \nitm\helpers\Form::getVariables($this->model, $options, [], false);

        return $this->render($options['form']['view'], array_merge([
            'model' => $this->model,
        ], $options['form']['args']));
    }
}
