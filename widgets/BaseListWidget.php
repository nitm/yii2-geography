<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, Wukdo.com, 2014
 * @version 1.0.0
 */

namespace nitm\geography\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use wukdo\models\Image;
use wukdo\models\MixologistList as ListModel;
use wukdo\helpers\Storage;
use wukdo\widgets\assets\MixologistListAsset;
use nitm\helpers\Helper;
use nitm\helpers\Response;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@wukdo.com>
 * @since 1.0
 */
abstract class BaseList extends \yii\base\Widget
{
	public $model;
	public $listModel;
	public $withForm;
	public $options = [
		'class' => 'clearfix',
	];

	protected $listView;
	protected $assetClass;
	protected $formWidgetClass;
	protected $listDataAttribute;
	protected $dataProvider;

	public function init()
	{
		parent::init();
		$this->options = array_merge($this->defaultOptions, $this->options);

		if(!isset($this->dataProvider) && !isset($this->listDataAttribute))
			throw new \yii\base\InvalidConfigException("You need to either specify the listDataAttribute or a dataProvider");
		if(!isset($this->listView))
			throw new \yii\base\InvalidConfigException("The listView attribute needs to be set to a valid view");

		if(!isset($this->listModel))
			$this->listModel = \Yii::createObject([
				'class' => $this->listClass,
				'remote_id' => $this->model->getId(),
				'remote_type' => $this->model->isWhat()
			]);
		$assetClass = $this->assetClass;
		$assetClass::register($this->getView());
	}

	protected function getDefaultOptions()
	{
		return [
			'role' => Inflector::singularize(lcfirst($this->listModel->formName())).'FormContainer',
			'id' => Inflector::singularize($this->listModel->isWhat()).'-form-container'
		];
	}

    /**
     * Runs the widget
     */
    public function run()
    {
		if(!isset($this->dataProvider))
	        $this->dataProvider = new ArrayDataProvider([
	            'allModels' => $this->model->{$this->listDataAttribute},
	        ]);
		$formWidget = $this->withForm == true ? \Yii::createObject([
			'class' => $this->formWidgetClass,
			'model' => $this->model
		]) : '';
		$list = Html::tag('div',
			$formWidget->run().$this->render($this->listView, [
				'model' => $this->listModel,
				'primaryModel' => $this->model,
				'dataProvider' => $this->dataProvider,
			]),
			$this->options);
		return $list;
    }
}
