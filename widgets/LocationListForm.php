<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, NitmGeography.com, 2014
 *
 * @version 1.0.0
 */

namespace nitm\geography\widgets;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@nitm\geography.com>
 *
 * @since 1.0
 */
class LocationListForm extends BaseListForm
{
    public $listItemType = 'address';
    public $listClass = '\nitm\geography\models\LocationList';
    protected $formView = '@nitm/geography/views/location-list/_form';

    public function init()
    {
        $this->assetClass = \nitm\geography\widgets\assets\AddressAsset::class;
        if ($this->type == 'external') {
            \nitm\geography\widgets\assets\GoogleMapsAsset::register($this->view);
        }
        parent::init();
        $this->options['action'] = \Yii::$app->urlManager->createUrl([
           '/nitm-geography/location-list/create',
        ]);
    }
}
