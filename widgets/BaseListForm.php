<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, Wukdo.com, 2014
 *
 * @version 1.0.0
 */

namespace nitm\geography\widgets;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@wukdo.com>
 *
 * @since 1.0
 */
abstract class BaseListForm extends \yii\base\Widget
{
    public $model;
    public $listModel;
    public $listItemType;
    public $type;
    public $assetClass;
    public $options = [];

    /**
     * @var string
     */
    protected $formView;

    /**
     * The list model class.
     *
     * @var string
     */
    protected $listClass;

    /**
     * The title for the form displayed.
     *
     * @var string
     */
    public $title;

    public function init()
    {
        parent::init();
        if (!isset($this->formView)) {
            throw new \yii\base\InvalidConfigException('The formView attribute needs to be set to a valid view');
        }
        if (!$this->listModel) {
            $this->listModel = \Yii::createObject([
                'class' => $this->listClass,
                'remote_id' => $this->model->getId(),
                'remote_type' => $this->model->isWhat(),
            ]);
        }
        if (class_exists($this->assetClass)) {
            $assetClass = $this->assetClass;
            $assetClass::register($this->getView());
        }
    }

    public function run()
    {
        return $this->render($this->formView, [
            'model' => $this->listModel,
            'primaryModel' => $this->model,
            'title' => $this->title ?: 'Create new list item',
            'listItemType' => $this->listItemType ?: $this->listModel->isWhat(),
            'action' => 'Update',
            'formOptions' => $this->options,
            'type' => $this->type,
        ]);
    }
}
