<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, NitmGeography.com, 2014
 *
 * @version 1.0.0
 */

namespace nitm\geography\widgets;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@nitm\geography.com>
 *
 * @since 1.0
 */
class CreateAddressForm extends BaseListForm
{
    public $listItemType = 'address';
    public $listClass = '\nitm\geography\models\LocationList';
    public $formView = '@nitm/geography/views/address/address';
}
