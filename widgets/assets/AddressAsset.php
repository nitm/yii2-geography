<?php
/**
 * @link http://www.nitm\geography.com/
 *
 * @copyright Copyright (c) 2014 NitmGeography
 */

namespace nitm\geography\widgets\assets;

use yii\web\AssetBundle;

/**
 * @author Malcolm Paul <lefteyecc@nitm.com>
 */
class AddressAsset extends AssetBundle
{
    public $baseUrl = null;
    public $sourcePath = '@nitm/geography/widgets/assets/';
    public $css = [
    ];
    public $js = [
        'js/address.js',
    ];
    public $depends = [
        'nitm\assets\AppAsset',
        'nitm\geography\widgets\assets\GoogleMapsAsset',
    ];
}
