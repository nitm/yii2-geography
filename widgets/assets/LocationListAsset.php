<?php
/**
 * @link http://www.nitm\geography.com/
 *
 * @copyright Copyright (c) 2014 NitmGeography
 */

namespace nitm\geography\widgets\assets;

use yii\web\AssetBundle;

/**
 * @author Malcolm Paul <lefteyecc@nitm.com>
 */
class LocationListAsset extends AssetBundle
{
    public $sourcePath = '@nitm/geography/widgets/assets/';
    public $js = [
      'js/location-list.js',
    ];
    public $depends = [
        'nitm\assets\AppAsset',
    ];

    public function init()
    {
        parent::init();
        \nitm\assets\SortableAsset::register(\Yii::$app->getView());
    }
}
