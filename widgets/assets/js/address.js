'use strict';

/*
 *
 *Google maps address api and other address helper tools
 */

class NitmGeographyAddress extends NitmEntity {
	constructor() {
		super({
			id: 'address',
			defaultInit: [
				'initAfterGoogleMapsLoads',
			],
			elements: {
				address: '[role~="mapAddress"]',
				synonyms: '[name="Address\[synonyms\]"]',
				inputs: {
					street: '[role~="mapStreetAddress"]',
					neighborhood: '[role~="mapNeighborhood"]',
					city: '[role~="mapCity"]',
					state: '[role~="mapState"]',
					country: '[role~="mapCountry"]',
					zipcode: '[role~="mapZipcode"]',
					latitude: '[role~="mapLatitude"]',
					longitude: '[role~="mapLongitude"]',
				}
			},
			views: {
				map: '[role~="map"]'
			}
		});
		this.places = {};
		this.infoWindows = {};
		this.markers = {};
		this.address = {};
	}

	initCategories() {
		//disable links to images for drinks extra images
		$("ul[id^='address_category']").find('li > label').find('a').on('click', (e) => {
			e.preventDefault();
		});
	}

	initAfterGoogleMapsLoads(containerId) {
		var resolveFunc = null;
		let module = this;
		var promise = new Promise(function(resolve, reject) {
			resolveFunc = resolve;
		});
		promise.then(function() {
			console.log('[Nitm][' + module.id + '] was waiting for google maps: loaded');
		});

		window.initMap = () => {
			resolveFunc();
			module.initMaps(containerId);
			module.initAutoComplete(containerId);
			module.initCategories(containerId);
		}
	}

	initMaps(containerId) {
		let $container = $nitm.getObj(containerId || 'body');
		$container.find(this.views.map).each((i, elem) => {
			let $elem = $(elem),
				data = $elem.data('address'),
				coords = {
					lat: parseFloat(data.latitude),
					lng: parseFloat(data.longitude),
				};
			let map = new google.maps.Map(elem, {
				center: coords,
				zoom: data.zoom || 17
			});
			this.setInfoWindow(data, this.getMarker(data, map));
			$elem.data('map', map);
		});
	}

	initAutoComplete(containerId) {
		let $container = $nitm.getObj(containerId || 'body');
		$container.find(this.elements.address).each((i, elem) => {
			let $elem = $(elem),
				map = this.getMap($elem.data('map-id'));

			let types = document.getElementById('type-selector');

			let autocomplete = new google.maps.places.SearchBox(elem);
			autocomplete.bindTo('bounds', map);

			autocomplete.addListener('places_changed', () => {
				let place = autocomplete.getPlaces().pop();
				if (!place.geometry) {
					window.alert("Autocomplete's returned place contains no geometry");
					return;
				}

				// If the place has a geometry, then present it on a map.
				if (place.geometry.viewport) {
					map.fitBounds(place.geometry.viewport);
				} else {
					map.setCenter(place.geometry.address);
					map.setZoom(17); // Why 17? Because it looks good.
				}
				elem.value = place.formatted_address;
				this.address = place;
				this.updateFormAddress(place.address_components, place.geometry.location);
				this.setInfoWindow(place, this.getMarker(place, map));
			});

			// Sets a listener on a radio button to change the filter type on Places
			// Autocomplete.
			function setupClickListener(id, types) {
				let radioButton = document.getElementById(id);
				radioButton.addEventListener('click', () => {
					autocomplete.setTypes(types);
				});
			}

			setupClickListener('changetype-all', []);
			setupClickListener('changetype-address', ['address']);
			setupClickListener('changetype-establishment', ['establishment']);
			setupClickListener('changetype-geocode', ['geocode']);
		});
	}

	extractParts(components, coords) {
		let ret_val = {
			longitude: coords.lng(),
			latitude: coords.lat(),
			street: components[0].short_name + ' ' + components[1].short_name,
		};
		components.forEach((elem, index) => {
			if (elem.types.includes('sublocality_level_1') !== false || elem.types.includes('sublocality') !== false) {
				ret_val['city'] = elem.short_name
			} else if (elem.types.includes('administrative_area_level_1') !== false) {
				ret_val['state'] = elem.long_name
			} else if (elem.types.includes('country') !== false) {
				ret_val['country'] = elem.long_name
			} else if (elem.types.includes('postal_code') !== false) {
				ret_val['zipcode'] = elem.short_name
			}
		});
		return ret_val;
	}

	formatAddress(address, localName) {
		let parts = address.split(',');
		if (parts.length == 5) {
			$nitm.getObj(this.elements.synonyms).val(parts.shift());
		}
		parts.unshift(localName);
		return parts.join(', ');
	}

	updateFormAddress(components, coords) {
		let parts = this.extractParts(components, coords);
		return new Promise((resolve, reject) => {
			let $form =
				$("form[role~='createAddressAddress'], form[role~='updateAddressAddress']");
			if ($form.data('address-is-formatted'))
				resolve();
			let coords = [
			'_save[latitude]=' + parts.latitude,
			'_save[longitude]=' + parts.longitude
		];
			$.get('/nitm-geography/country/filter?name=' + parts.country, (result) => {
				parts.country = result.data[0].id;
				$.get('/nitm-geography/state/filter?' + [
				'country_id=' + parts.country,
				'name=' + parts.state,
			].join('&') + '&' + coords.join('&'), (result) => {
					parts.state = result.data[0].id;
					$.get('/nitm-geography/city/filter?' + [
					'country_id=' + parts.country,
					'state_id=' + parts.state,
					'zipcode=' + parts.zipcode,
					'name=' + parts.city,
				].join('&') + '&' + coords.join('&'), (result) => {
						parts.city = result.data[0].id;
						for (let key in parts) {
							$(this.elements.inputs[key]).val(parts[key]);
						}
						$form.data('address-is-formatted', true);
						$form.trigger('submit');
						resolve();
					});
				});
			});
		});
	}

	getMap(id) {
		return $nitm.getObj(id).data('map');
	}

	getMarker(place, map) {
		if (!this.places.hasOwnProperty(place.id))
			this.places[place.id] = place;
		if (!this.markers.hasOwnProperty(place.id)) {
			this.markers[place.id] = new google.maps.Marker({
				position: map.center,
				map: map,
				animation: google.maps.Animation.DROP,
				label: place.name,
				name: place.name,
			});
		}
		return this.markers[place.id];
	}

	setMarker(place, map) {
		let marker = this.getMarker(place, map);
		if (marker.name) {
			marker.setIcon( /** @type {google.maps.Icon} */ ({
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(35, 35)
			}));
			marker.setPosition(place.geometry.address);
			marker.setVisible(true);
		}
	}

	getPlace(address) {
		let place = null;
		if (place.address_components) {
			address = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || '')
	  ].join(' ');
		}
	}

	setInfoWindow(place, marker) {
		let infoWindow = this.getInfoWindow(marker);
		if (!infoWindow) {
			//if(place === undefined)
			//	place = this.getPlace($elem.data('address'));
			infoWindow = new google.maps.InfoWindow({
				content: this.getInfoWindowContent(place)
			});
			this.infoWindows[this.getInfoWindowId(marker)] = infoWindow;
		}
		infoWindow.open(marker.map, marker);
		return infoWindow;
	}

	getInfoWindowContent(data) {
		return '<div>' +
			'<h1>' + data.name + '</h1>' +
			'<p>' +
			data.name +
			'</p>' +
			'</div>';
	}

	getInfoWindowId(marker) {
		return 'infoWindow' + marker.name;
	}

	getInfoWindow(marker) {
		return this.infoWindows[this.getInfoWindowId(marker)];
	}
}
$nitm.onModuleLoad('entity', function(module) {
	module.initModule(new NitmGeographyAddress());
});
