'use strict';

class NitmGeographyLocationList extends BaseList {
	constructor() {
		super({
			id: 'location-list',
			forms: {
				roles: {
					create: 'createLocationItem',
					update: 'updateLocationItem'
				}
			},
			buttons: {
				create: 'newLocationItem',
				remove: 'removeLocationItem',
				disable: 'disableLocationItem',
			},
			views: {
				listFormContainerId: "[role~='locationListFormContainer']",
				containerId: "[role~='locationList']",
				itemId: "location-list"
			}
		});
	}
}

$nitm.onModuleLoad('entity', function(module) {
	module.initModule(new NitmGeographyLocationList());
});
