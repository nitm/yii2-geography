<?php
/**
 * @link http://www.nitm\geography.com/
 *
 * @copyright Copyright (c) 2014 NitmGeography
 */

namespace nitm\geography\widgets\assets;

use yii\web\AssetBundle;

/**
 * @author Malcolm Paul <lefteyecc@nitm.com>
 */
class GoogleMapsAsset extends AssetBundle
{
    public $baseUrl = null;
    public $sourcePath = '@nitm/geography/widgets/assets/';
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
    ];

    public function init()
    {
        parent::init();
        $this->setGoogleMapsOptions(\Yii::$app->getModule('nitm-geography')->googleOptions, \Yii::$app->view);
    }

    public function setGoogleMapsOptions($options, $view = null)
    {
        $view = $view ?: \Yii::$app->view;
        extract($options);
        $googleMaps = '//maps.googleapis.com/maps/api/js?';
        $googleMaps .= http_build_query([
            'key' => $key,
            'libraries' => implode(',', $libraries),
            'callback' => 'initMap',
        ]);
        $this->js[] = $googleMaps;

        if (isset($helper)) {
            $this->js[] = $helper;
        }
    }
}
