<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, Wukdo.com, 2014
 *
 * @version 1.0.0
 */

namespace nitm\geography\widgets;

use yii\helpers\Inflector;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * Provides a basis for easily creating list views.
 *
 * @author Malcolm Paul <lefteyecc@wukdo.com>
 *
 * @since 1.0
 */
abstract class BaseList extends \yii\base\Widget
{
    public $model;
    public $listModel;
    public $withForm;
    public $formType;
    public $options = [];
    public $formWidgetClass;
    public $listClass;
    public $listView;

    protected $assetClass;
    protected $arrayDataProvider;
    protected $queryDataProvider;
    protected $dataProvider;

    public function init()
    {
        parent::init();

        if (!isset($this->dataProvider) && !isset($this->queryDataProvider) && !isset($this->arrayDataProvider) && !$this->hasMethod('getDataProvider')) {
            throw new \yii\base\InvalidConfigException('You need to either specify one of the following:  $this->arrayDataProvider, $this->queryDataProvider, $this->getDataProvider function or a custom $this->dataProvider');
        }
        if (!isset($this->listView)) {
            throw new \yii\base\InvalidConfigException('The listView attribute needs to be set to a valid view');
        }

        if (!isset($this->listModel)) {
            $this->listModel = \Yii::createObject([
                'class' => $this->listClass,
                'remote_id' => $this->model->getId(),
                'remote_type' => $this->model->isWhat(),
            ]);
        }

        $this->options = array_merge($this->defaultOptions, $this->options);
        if (class_exists($this->assetClass)) {
            $assetClass = $this->assetClass;
            $assetClass::register($this->getView());
        }
    }

    protected function getDefaultOptions()
    {
        return [
            'class' => 'clearfix',
            'role' => Inflector::singularize(lcfirst($this->listModel->formName())).'FormContainer',
            'id' => Inflector::singularize($this->listModel->isWhat()).'-form-container',
        ];
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        if (!isset($this->dataProvider)) {
            if ($this->hasMethod('getDataProvider')) {
                $this->dataProvider = call_user_func_array([$this, 'getDataProvider'], [$this->model]);
            } elseif (isset($this->queryDataProvider)) {
                $this->dataProvider = new ActiveDataProvider([
                     'query' => call_user_func([$this->model, $this->queryDataProvider]),
                 ]);
            } else {
                if (method_exists($this->model, $this->arrayDataProvider) && is_callable([$this->model, $this->arrayDataProvider])) {
                    $models = call_user_func([$this->model, $this->arrayDataProvider]);
                } else {
                    $models = $this->model->{$this->arrayDataProvider};
                }
                $this->dataProvider = new ArrayDataProvider([
                     'allModels' => $models,
                 ]);
            }
        }

        $list = $this->render($this->listView, [
            'model' => $this->listModel,
            'primaryModel' => $this->model,
            'dataProvider' => $this->dataProvider,
            'withForm' => $this->withForm,
            'formType' => $this->formType,
            'widgetFormClass' => $this->formWidgetClass,
            'options' => $this->options,
            'widget' => $this,
        ]);

        return $list;
    }

    public function importNavFor($type)
    {
        $imports = $this->model->getImportJobs($type, false)->asArray()->all();
        $importPreview = '';
        if ($imports != []) {
            $importPreview = \yii\bootstrap\ButtonDropdown::widget([
                'label' => 'Import Jobs',
                'dropdown' => [
                    'items' => array_map(function ($source) {
                        $v = [
                            'url' => \Yii::$app->urlManager->createUrl([
                                '/import/view/'.$source['id'],
                                '_format' => 'modal',
                            ]),
                            'class' => 'btn-info',
                            'label' => ($source['total'] ? 'View Import: ' : 'Start Import: ').$source['name'],
                        ];
                        $widget = new \nitm\widgets\modal\Modal([
                            'size' => 'x-large',
                            'contentOnly' => true,
                            'toggleButton' => [
                                'tag' => false,
                                'class' => '',
                                'label' => $v['label'],
                            ],
                        ]);
                        $v['linkOptions'] = array_merge($widget->toggleButton, [
                            'data-url' => $v['url'],
                        ]);
                        $widget->run();

                        return $v;
                    }, $imports),
                ],
            ]);
        }

        return $importPreview;
    }
}
