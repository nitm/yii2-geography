<?php

namespace nitm\geography\models;

use Yii;

/**
 * This is the model class for table "nitm_geography_address".
 *
 * @property int $id
 * @property string $title
 * @property string $address
 * @property string $latitude
 * @property string $longitude
 * @property int $city_id
 * @property int $state_id
 * @property int $country_id
 * @property int $neighborhood_id
 * @property string $created_at
 * @property string $deleted_at
 * @property City $city
 * @property Country $country0
 * @property Neighborhood $neighborhood
 * @property State $state0
 * @property LocationList[] $nitmGeographyLocationLists
 * @property AddressMetadata[] $nitmGeographyAddressMetadatas
 */
class Address extends \nitm\geography\models\Entity
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_geography_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['address', 'latitude', 'longitude', 'city_id', 'state_id', 'country_id'], 'required'],
            [['latitude', 'longitude'], 'number'],
            [['city_id', 'state_id', 'country_id', 'neighborhood_id'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
            [['title'], 'string', 'max' => 128],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['neighborhood_id'], 'exist', 'skipOnError' => true, 'targetClass' => Neighborhood::className(), 'targetAttribute' => ['neighborhood_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'address' => Yii::t('app', 'Address'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'city_id' => Yii::t('app', 'City'),
            'state_id' => Yii::t('app', 'State'),
            'country_id' => Yii::t('app', 'Country'),
            'neighborhood_id' => Yii::t('app', 'Neighborhood'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id'])->inverseOf('addresses');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])->inverseOf('addresses');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeighborhood()
    {
        return $this->hasOne(Neighborhood::className(), ['id' => 'neighborhood_id'])->inverseOf('addresses');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id'])->inverseOf('addresses');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasMany(LocationList::className(), ['address_id' => 'id'])->inverseOf('address');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadata()
    {
        return $this->hasMany(AddressMetadata::className(), ['content_id' => 'id'])->inverseOf('content');
    }
}
