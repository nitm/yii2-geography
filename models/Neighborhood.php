<?php

namespace nitm\geography\models;

use Yii;

/**
 * This is the model class for table "neighborhood".
 *
 * @property int $id
 * @property int $name
 * @property int $city_id
 * @property int $state_id
 * @property int $country_id
 * @property string $longitude
 * @property int $latitude
 * @property City $city
 * @property State $state
 * @property Country $country
 */
class Neighborhood extends Entity
{
    protected $is = 'neighborhood';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_geography_neighborhood';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(),  [
            [['name', 'city_id', 'state_id', 'country_id', 'longitude', 'latitude'], 'required'],
            [['name', 'city_id', 'state_id', 'country_id', 'latitude'], 'integer'],
            [['longitude'], 'number'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'city_id' => Yii::t('app', 'City ID'),
            'state_id' => Yii::t('app', 'State ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'longitude' => Yii::t('app', 'Longitude'),
            'latitude' => Yii::t('app', 'Latitude'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['name', 'city_id', 'latitude', 'longitude', 'country_id', 'state_id'];
        $scenarios['create'] = ['name', 'city_id', 'latitude', 'longitude', 'country_id', 'state_id'];

        return $scenarios;
    }
}
