<?php

namespace nitm\geography\models;

/**
 * This is the Entity base glass for NITM Geography.
 */
class Entity extends \nitm\models\Entity
{
    use \nitm\geography\traits\Relations;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['latitude', 'filterCoord'],
            ['longitude', 'filterCoord'],
      ];
    }

    public function getTitleAttribute()
    {
        return 'name';
    }

    public function filterCoord($coord)
    {
        return number_format((float) $coord, 6, '.', '');
    }
}
