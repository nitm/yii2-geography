<?php

namespace nitm\geography\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property string $name
 * @property string $state
 * @property string $zipcode
 * @property string $latitude
 * @property string $longitude
 * @property string $county
 * @property int $state_id
 * @property int $country_id
 * @property int $id
 * @property Neighborhood[] $neighborhoods
 * @property Country $country
 * @property State $state
 */
class City extends Entity
{
    protected $is = 'city';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_geography_city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['name', 'zipcode', 'latitude', 'longitude'], 'required'],
            [['name', 'zipcode'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['state_id', 'country_id'], 'integer'],
            [['name', 'country_id', 'state_id'], 'unique', 'targetAttribute' => ['latitude', 'longitude'], 'message' => 'A city already exists for these coordinates'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'zipcode' => Yii::t('app', 'Zip'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'state_id' => Yii::t('app', 'State'),
            'country_id' => Yii::t('app', 'Country'),
            'id' => Yii::t('app', 'ID'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['name', 'zipcode', 'latitude', 'longitude', 'country_id', 'state_id'];
        $scenarios['create'] = ['name', 'zipcode', 'latitude', 'longitude', 'country_id', 'state_id'];

        return $scenarios;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeighborhoods()
    {
        return $this->hasMany(Neighborhood::className(), ['city_id' => 'id'])
            ->orderBy(['name' => SORT_ASC]);
    }
}
