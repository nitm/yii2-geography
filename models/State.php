<?php

namespace nitm\geography\models;

use Yii;

/**
 * This is the model class for table "state".
 *
 * @property string $name
 * @property string $code
 * @property int $country_id
 * @property int $id
 * @property Country $country
 * @property Neighborhood[] $neighborhoods
 * @property City[] $cities
 */
class State extends Entity
{
    protected $is = 'nitm_geography_state';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_geography_state';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['country_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['code'], 'string', 'max' => 2],
            [['name', 'country_id'], 'unique', 'targetAttribute' => ['name', 'country_id'], 'message' => 'The combination of Name and Country ID has already been taken.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'country_id' => Yii::t('app', 'Country ID'),
            'id' => Yii::t('app', 'ID'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['name', 'code', 'country_id'];
        $scenarios['create'] = ['name', 'code', 'country_id'];

        return $scenarios;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeighborhoods()
    {
        return $this->hasMany(Neighborhood::className(), ['state_id' => 'id'])
            ->orderBy(['name' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['state_id' => 'id'])
            ->orderBy(['name' => SORT_ASC]);
    }
}
