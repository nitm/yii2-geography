<?php

namespace nitm\geography\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $code
 * @property string $clean_name
 * @property string $name
 * @property string $iso3_code
 * @property int $numcode
 * @property int $phonecode
 * @property State[] $states
 * @property Neighborhood[] $neighborhoods
 * @property City[] $cities
 */
class Country extends Entity
{
    protected $is = 'country';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_geography_country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'clean_name', 'name', 'phonecode'], 'required'],
            [['numcode', 'phonecode'], 'integer'],
            [['code'], 'string', 'max' => 2],
            [['clean_name', 'name'], 'string', 'max' => 80],
            [['iso3_code'], 'string', 'max' => 3],
            [['code', 'clean_name', 'name', 'iso3_code'], 'unique', 'targetAttribute' => ['code', 'clean_name', 'name', 'iso3_code'], 'message' => 'The combination of Code, Clean Name, Name and Iso3 Code has already been taken.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'clean_name' => Yii::t('app', 'Clean Name'),
            'name' => Yii::t('app', 'Name'),
            'iso3_code' => Yii::t('app', 'Iso3 Code'),
            'numcode' => Yii::t('app', 'Numcode'),
            'phonecode' => Yii::t('app', 'Phonecode'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['code', 'name', 'clean_name', 'iso3_code', 'numcode', 'phoncode'];
        $scenarios['create'] = ['code', 'name', 'clean_name', 'iso3_code', 'numcode', 'phoncode'];

        return $scenarios;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStates()
    {
        return $this->hasMany(State::className(), ['country_id' => 'id'])
            ->orderBy(['name' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeighborhoods()
    {
        return $this->hasMany(Neighborhood::className(), ['country_id' => 'id'])
            ->orderBy(['name' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['country_id' => 'id'])
            ->orderBy(['name' => SORT_ASC]);
    }
}
