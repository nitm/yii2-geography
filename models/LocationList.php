<?php

namespace nitm\geography\models;

use Yii;
use nitm\models\User;

/**
 * This is the model class for table "nitm_geography_address_list".
 *
 * @property int $id
 * @property int $author_id
 * @property string $created_at
 * @property int $editor_id
 * @property string $updated_at
 * @property int $remote_id
 * @property string $remote_type
 * @property string $remote_class
 * @property int $address_id
 * @property int $priority
 * @property bool $deleted
 * @property bool $is_primary
 * @property string $deleted_at
 * @property int $deleted_by
 * @property bool $disabled
 * @property string $disabled_at
 * @property int $disabled_by
 * @property Address $address
 * @property User $author
 * @property User $editor
 * @property LocationListMetadata[] $nitmGeographyLocationListMetadatas
 */
class LocationList extends \nitm\geography\models\Entity
{
    protected $_remote;
    protected $_address;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_geography_address_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['remote_id'], 'verifyAddress'],
            [['remote_id', 'remote_type', 'remote_class'], 'required'],
            [['author_id', 'editor_id', 'remote_id', 'address_id', 'priority', 'deleted_by', 'disabled_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'disabled_at'], 'safe'],
            [['remote_class'], 'string'],
            [['deleted', 'is_primary', 'disabled'], 'boolean'],
            [['remote_type'], 'string', 'max' => 32],
            [['remote_id', 'remote_type', 'address_id'], 'unique', 'targetAttribute' => ['remote_id', 'remote_type', 'address_id'], 'message' => 'This address has already been added!'],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['editor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['editor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'author_id' => Yii::t('app', 'Author'),
            'created_at' => Yii::t('app', 'Created At'),
            'editor_id' => Yii::t('app', 'Editor'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'remote_id' => Yii::t('app', 'Remote'),
            'remote_type' => Yii::t('app', 'Remote Type'),
            'remote_class' => Yii::t('app', 'Remote Class'),
            'address_id' => Yii::t('app', 'Address'),
            'priority' => Yii::t('app', 'Priority'),
            'deleted' => Yii::t('app', 'Deleted'),
            'is_primary' => Yii::t('app', 'Is Primary'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'disabled' => Yii::t('app', 'Disabled'),
            'disabled_at' => Yii::t('app', 'Disabled At'),
            'disabled_by' => Yii::t('app', 'Disabled By'),
        ];
    }

    public function title()
    {
        return $this->address()->title;
    }

    public function verifyAddress($attribute, $params)
    {
        if ($this->address()->validate()) {
            $this->address->save();
        }
        $this->address_id = $this->address->id;
        if (!$this->address_id) {
            $this->addError($attribute, $this->address->formatErrors());
        }
    }

    public function setRemote($remote)
    {
        $this->_remote = $remote;
    }

    public function getRemote()
    {
        if (!isset($this->_remote)) {
            $class = $this->remote_class;
            if (class_exists($class)) {
                $remote = $class::findOne($this->remote_id);
                if ($remote) {
                    $this->_remote = $remote;
                } else {
                    $this->_remote = new static();
                }
            } else {
                $this->_remote = new static();
            }
        }

        return $this->_remote;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id'])->inverseOf('location');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadata()
    {
        return $this->hasMany(LocationListMetadata::className(), ['content_id' => 'id'])->inverseOf('address');
    }
}
