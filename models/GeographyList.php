<?php

namespace nitm\geography\models;

use Yii;

/**
 * This is the model class for table "geography_list".
 *
 * @property int $id
 * @property int $author_id
 * @property string $created_at
 * @property int $editor_id
 * @property string $updated_at
 * @property int $remote_id
 * @property string $remote_type
 * @property string $remote_class
 * @property int $parent_id
 * @property int $priority
 * @property bool $deleted
 * @property string $deleted_at
 * @property int $deleted_by
 * @property bool $disabled
 * @property string $disabled_at
 * @property int $disabled_by
 * @property string $parent_type
 * @property string $parent_class
 * @property GeographyListMetadata[] $geographyListMetadatas
 * @property User $author
 * @property User $editor
 */
class GeographyList extends \nitm\geography\models\Entity
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_geography_geography_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_id', 'remote_id', 'remote_type', 'remote_class', 'parent_id', 'parent_type', 'parent_class'], 'required'],
            [['author_id', 'editor_id', 'remote_id', 'parent_id', 'priority', 'deleted_by', 'disabled_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'disabled_at'], 'safe'],
            [['remote_class', 'parent_class'], 'string'],
            [['deleted', 'disabled'], 'boolean'],
            [['remote_type', 'parent_type'], 'string', 'max' => 32],
            [['remote_id', 'remote_type', 'parent_id'], 'unique', 'targetAttribute' => ['remote_id', 'remote_type', 'parent_id'], 'message' => 'The combination of Remote ID, Remote Type and Parent ID has already been taken.'],
        ];
    }

    public function scenarios()
    {
        return [
            'create' => ['remote_id', 'remote_type', 'remote_class', 'parent_id', 'parent_type', 'parent_class', 'author_id'],
            'update' => ['remote_id', 'remote_type', 'remote_class', 'parent_id', 'parent_type', 'parent_class', 'author_id'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'author_id' => Yii::t('app', 'Author ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'editor_id' => Yii::t('app', 'Editor ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'remote_id' => Yii::t('app', 'Remote ID'),
            'remote_type' => Yii::t('app', 'Remote Type'),
            'remote_class' => Yii::t('app', 'Remote Class'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'priority' => Yii::t('app', 'Priority'),
            'deleted' => Yii::t('app', 'Deleted'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'disabled' => Yii::t('app', 'Disabled'),
            'disabled_at' => Yii::t('app', 'Disabled At'),
            'disabled_by' => Yii::t('app', 'Disabled By'),
            'parent_type' => Yii::t('app', 'Parent Type'),
            'parent_class' => Yii::t('app', 'Parent Class'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeographyListMetadatas()
    {
        return $this->hasMany(GeographyListMetadata::className(), ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEditor()
    {
        return $this->hasOne(User::className(), ['id' => 'editor_id']);
    }
}
