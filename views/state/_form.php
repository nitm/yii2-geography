<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\icons\Icon;

/**
 * @var yii\web\View $this
 * @var lab1\models\Carrier $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="carrier-form">
	<div class="row">
		<div class="col-md-12 col-lg-12">

    <?php $form = ActiveForm::begin(); ?>

	<?=
		$form->field($model, 'parent_id')->widget(Select2::className(), [
			'data' => $markets,
		])->label("Parent");
	?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'description')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
