<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var lab1\models\Market $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Market',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Markets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="carrier-create">
	<?= \yii\widgets\Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]); ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'markets' => @$markets
    ]) ?>

</div>
