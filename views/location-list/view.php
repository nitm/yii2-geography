<?php

use yii\helpers\Html;
use nitm\helpers\Icon;

/**
 * @var yii\web\View
 * @var nitm\geography\models\MoodItems $model
 * @var yii\widgets\ActiveForm          $form
 */
$itemId = 'location-list'.$model->remote_type.$model->remote_id.$model->address()->id;
?>
<?= Html::beginTag('li', [
    'class' => 'media locationlist-item '.\nitm\helpers\Statuses::getListIndicator($model->getStatus()),
    'data-url' => '/location-list/set-priority/'.$model->getId().'?_format=json',
    'id' => $itemId,
    'role' => 'statusIndicator'.$model->getId(),
]); ?>
	<div class="media-left">
		<a href="#">
			<span class="media-object">
				<?= ''
               // \nitm\filemanager\widgets\Thumbnail::widget([
               //      'model' => $model->icon(),
               //      'size' => 'large',
               //      'options' => [
               //          'class' => 'text-center',
               //      ],
               //  ]);
            ?>
			</span>
		</a>
	</div>
	<div class="media-body">
		<div class="row">
			<div class="col-sm-12 col-lg-10 col-md-10">
				<h4 class="media-heading"><?= $model->address()->title(); ?></h4>
				<p><?= $model->address()->address; ?></p>
			</div>
		</div>
	</div>
	<div class="media-right">
		<?php
            echo Html::a(Icon::forAction('delete'), '/'.implode('/', [
                    $model->isWhat(),
                    'delete',
                    $model->remote_type,
                    $model->remote_id,
                    $model->address()->id,
                ]).'?do=true', [
                'class' => 'fa-2x '.($model->isNewRecord ? 'hidden' : ''),
                'title' => Yii::t('yii', 'Remove '.$model->address()->title().' from list'),
                'role' => 'metaAction deleteAction',
                'data-method' => 'post',
                'data-parent' => '#'.$itemId,
                'data-pjax' => 0,
                'data-module' => 'location-list',
                'data-data' => json_encode([
                    $model->formName() => [
                        'remote_id' => $model->remote_id,
                        'remote_type' => $model->remote_type,
                        'address_id' => $model->address()->id,
                    ],
                ]),
            ]);
        ?>
	</div>
</li>
