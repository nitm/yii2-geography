<?php

use yii\helpers\Html;

/**
 * @var yii\web\View
 * @var yii\data\ActiveDataProvider           $dataProvider
 * @var nitm\geography\models\search\MoodList $searchModel
 */
$this->title = Yii::t('app', '{listType} for {type}: {title}', [
     'listType' => $model->properName(),
     'type' => $primaryModel->properName(),
     'title' => $primaryModel->title(),
]);
$this->params['breadcrumbs'][] = $this->title;

$options = array_merge([
    'id' => 'location-list-index',
    'role' => 'locationListFormContainer',
], (isset($options) ? $options : []));
?>
<?= Html::beginTag('div', $options); ?>
   <div class="row">
      <div class="col-lg-6 col-sm-12">
          <h1><?= Html::encode($this->title) ?></h1>
          <?php
              if (isset($withForm) && $withForm !== false) {
                  echo $widgetFormClass::widget([
                      'type' => $formType,
                      'model' => $primaryModel,
                  ]);
              }
          ?>
      </div>
      <div class="col-lg-6 col-sm-12">
          <?= $this->render('list', [
              'model' => $model,
              'primaryModel' => $primaryModel,
              'dataProvider' => $dataProvider,
              'widget' => $widget,
          ]); ?>
      </div>
   </div>
</div>
