<?php

use yii\helpers\Html;
use kartik\builder\Form;
use nitm\geography\models\Address;
use nitm\widgets\ajax\Dropdown;

/**
 * @var yii\web\View
 * @var nitm\geography\models\Address $model
 * @var yii\widgets\ActiveForm        $form
 */
$apiListUrl = '/nitm-geography/address/list';

?>
<?php

    echo Html::tag('h4', 'Add Addresss');
    echo $form->field($model, 'address_id')->widget(Dropdown::className(), [
        'options' => [
            'id' => 'location-list'.$primaryModel->getId(),
            'placeholder' => 'Search for addresses',
        ],
        'url' => $apiListUrl,
        'pluginEvents' => [
            'select2:select' => 'function (event) {
				"use strict";
				$(event.target.form).trigger("submit");
			}',
        ],
    ]);
?>

</div>
