<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use nitm\geography\models\LocationList;

/**
 * @var yii\web\View
 * @var yii\data\ActiveDataProvider               $dataProvider
 * @var nitm\geography\models\search\LocationList $searchModel
 */
$dataProvider->pagination = false;
$primaryModel = isset($primaryModel) ? $primaryModel : $model->getIngredient()->one();

?>
<div id="location-list-container">
	<?= Html::tag('h4', 'Update Location List'); ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => function ($model, $key, $index, $widget) use ($primaryModel) {
            $widget->itemOptions['id'] = 'location-list-item'.$model->getId();

            return $this->render('view', [
                'model' => $model,
                'primaryModel' => $primaryModel,
                'notAsListItem' => true,
            ]);
        },
        'itemOptions' => [
            'tag' => false,
            'class' => 'list-group-item',
        ],
        'summary' => false,
        'options' => [
            'role' => 'locationList',
            'id' => 'location-list-list',
            'tag' => 'ul',
        ],
    ]); ?>
</div>
