<?php

use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View
 * @var nitm\geography\models\Address $model
 * @var yii\widgets\ActiveForm        $form
 */
$action = $model->getIsNewRecord() ? 'create' : 'update';
$type = isset($type) ? $type : 'local';
?>

<div class="location-list-form">
<?php
    $form = ActiveForm::begin([
        'action' => "/nitm-geography/location-list/$action?_format=json",
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'options' => [
            'role' => 'createLocationItem',
        ],
        'formConfig' => [
            'showLabels' => false,
        ],
        'enableAjaxValidation' => true,
    ]);
    echo $form->field($model, 'remote_type')->hiddenInput([
        'value' => $primaryModel->isWhat(),
    ])->label('Remote Type', ['class' => 'sr-only']);
    echo $form->field($model, 'remote_id')->hiddenInput([
         'value' => $primaryModel->getId(),
    ])->label('Remote ID', ['class' => 'sr-only']);
    echo $form->field($model, 'remote_class')->hiddenInput([
          'value' => $primaryModel->className(),
    ])->label('Remote Class', ['class' => 'sr-only']);

    if ($type == 'external') {
        echo $this->render('_from_api', [
            'form' => $form,
            'model' => $model->address(),
            'listModel' => $model,
            'primaryModel' => $primaryModel,
         ]);
    } else {
        echo $this->render('_from_local', [
           'form' => $form,
           'primaryModel' => $primaryModel,
           'model' => $model,
        ]);
    }
    ActiveForm::end();
?>

</div>
