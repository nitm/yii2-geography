<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use kartik\builder\Form;

/**
 * @var yii\web\View
 * @var nitm\geography\models\Address $model
 * @var yii\widgets\ActiveForm        $form
 */
?>

<div id="address-form-container" class="clearfix">
<?php
    echo $form->field($listModel, 'address[address]', [
        'addon' => [
            'append' => [
                'asButton' => true,
                'content' => Html::submitButton(Icon::show('save').Html::tag('span', ' Save Address'), [
                    'class' => 'btn btn-info',
                    'type' => 'submit',
                ]),
            ],
        ],
    ])->textInput([
        'role' => 'mapAddress mapStreetAddress',
        'data-map-id' => 'map'.$listModel->getId(),
        'value' => $model->address,
        'title' => $model->title(),
        'placeholder' => 'Start typing to find an address',
    ])
    ->label('Address', ['class' => 'sr-only']);
?>
	<div class="hidden">
	<?php
        echo $form->field($listModel, 'address[latitude]')->hiddenInput([
           'role' => 'mapLatitude',
        ])->label('Latitude', ['class' => 'sr-only']);
        echo $form->field($listModel, 'address[longitude]')->hiddenInput([
           'role' => 'mapLongitude',
        ])->label('Longitude', ['class' => 'sr-only']);
        echo $form->field($listModel, 'address[state_id]')->hiddenInput([
           'role' => 'mapState',
        ])->label('State', ['class' => 'sr-only']);
        echo $form->field($listModel, 'address[city_id]')->hiddenInput([
           'role' => 'mapCity',
        ])->label('City', ['class' => 'sr-only']);
        echo $form->field($listModel, 'address[country_id]')->hiddenInput([
           'role' => 'mapCountry',
        ])->label('Country', ['class' => 'sr-only']);
        echo $form->field($listModel, 'address[zipcode]')->hiddenInput([
           'role' => 'mapZipcode',
        ])->label('Zipcode', ['class' => 'sr-only']);
        echo $form->field($listModel, 'address[neighborhood_id]')->hiddenInput([
           'role' => 'mapNeighborhood',
        ])->label('Neighborhood', ['class' => 'sr-only']);
    ?>
	</div>
	<div id="type-selector" class="controls">
	  <input type="radio" id="changetype-all" checked="checked">
	  <label for="changetype-all">All</label>

	  <input type="radio" id="changetype-establishment">
	  <label for="changetype-establishment">Establishments</label>

	  <input type="radio" id="changetype-address">
	  <label for="changetype-address">Addresses</label>

	  <input type="radio" id="changetype-geocode">
	  <label for="changetype-geocode">Geocodes</label>
	</div>
	<div id="map<?= $listModel->getId(); ?>" role="map" data-address='<?=json_encode($model->toArray())?>' style="height:500px">
	</div>
</div>
