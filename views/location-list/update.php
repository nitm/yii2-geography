<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var nitm\geography\models\Instructions $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Instructions',
]) . ' ' . $model->getId();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Instructions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getId(), 'url' => ['view', 'id' => $model->getId()]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div id="<?= $formOptions['container']['id']; ?>" class="<?= $formOptions['container']['class'] ?>">
	<?php if(!\Yii::$app->request->isAjax): ?>
	<?= \yii\widgets\Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]); ?>
	<h2><?= Html::encode($this->title) ?></h2>
	<?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
		'formOptions' => $formOptions,
		'scenario' => $scenario,
		'action' => $action,
		'type' => $type
    ]) ?>
</div>
