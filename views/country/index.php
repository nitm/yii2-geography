<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use nitm\helpers\Icon;

/**
 * @var yii\web\View
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var lab1\models\search\Country  $searchModel
 */
$this->title = Yii::t('app', 'Countries');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
<div class="country-index">
	<?= yii\widgets\Breadcrumbs::widget([
        'links' => $this->params['breadcrumbs'],
    ]); ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);?>

    <p>
        <?= Html::a(Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Country',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'striped' => false,
        'export' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'clean_name',
            'code',
            'iso3_code',

            [
                'class' => 'nitm\grid\ActionColumn',
                'template' => '{form/update} {disable}',
                'buttons' => [
                    'form/update' => function ($url, $model) {
                        return \nitm\widgets\modal\Modal::widget([
                            'size' => 'x-large',
                            'toggleButton' => [
                                'tag' => 'a',
                                'label' => Icon::forAction('update'),
                                'href' => \Yii::$app->urlManager->createUrl([$url, '_format' => 'modal']),
                                'title' => Yii::t('yii', 'Edit '),
                                'role' => 'disabledOnClose',
                                'data-pjax' => 0,

                            ],
                            'contentOptions' => [
                                'class' => 'modal-full',
                            ],
                            'dialogOptions' => [
                                'class' => 'modal-full',
                            ],
                        ]);
                    },

                    'view' => function ($url, $model) {
                        return \nitm\widgets\modal\Modal::widget([
                            'size' => 'large',
                            'toggleButton' => [
                                'tag' => 'a',
                                'label' => Icon::forAction('view'),
                                'href' => \Yii::$app->urlManager->createUrl([$url, '_format' => 'modal']),
                                'title' => Yii::t('yii', 'View '.$model->name),
                                'role' => 'disabledOnClose',
                                'data-pjax' => 0,
                            ],
                            'contentOptions' => [
                                'class' => 'modal-full',
                            ],
                            'dialogOptions' => [
                                'class' => 'modal-full',
                            ],
                        ]);
                    },
                  //   'disable' => function ($url, $model) {
                  //       return Html::a(Icon::forAction('disable', 'disabled', $model), $url, [
                  //           'title' => Yii::t('yii', ($model->disabled ? 'Enable' : 'Disable').' '.$model->isWhat().' '.$model->name),
                  //           'role' => 'metaAction disableAction',
                  //           'data-parent' => '#'.$model->isWhat().$model->getId(),
                  //           'data-pjax' => 0,
                  //           'data-method' => 'post',
                  //           'class' => 'fa-2x',
                  //       ]);
                  //   },
                  //   'approve' => function ($url, $model) {
                  //       return Html::a(Icon::forAction('approve', 'approved', $model), $url, [
                  //           'title' => Yii::t('yii', ($model->disabled ? 'Dissapprove' : 'Approve').' '.$model->isWhat().': '.$model->name),
                  //           'role' => 'metaAction resolveAction',
                  //           'data-parent' => '#'.$model->isWhat().$model->getId(),
                  //           'data-pjax' => 0,
                  //           'data-method' => 'post',
                  //           'class' => 'fa-2x',
                  //       ]);
                  //   },
                ],
                'urlCreator' => function ($action, $model) {
                    $params = [
                        '/'.$model->isWhat().'/'.$action.'/'.$model->id,
                    ];

                    return \yii\helpers\Url::toRoute($params);
                },
            ],
        ],
        'options' => [
            'id' => 'countries',
        ],
        'rowOptions' => function ($model, $key, $index, $grid) {
            return [
                'class' => 'item '.\nitm\helpers\Statuses::getIndicator($model->getStatus()),
                'style' => 'border-top:solid medium #CCC',
                'id' => $model->isWhat().$model->getId(),
                'role' => 'statusIndicator'.$model->getId(),
            ];
        },
        'pager' => [
            'class' => \nitm\widgets\ias\ScrollPager::className(),
            'overflowContainer' => '.content',
            'container' => '#countries',
            'item' => '.item',
            'negativeMargin' => 150,
            'delay' => 500,
        ],
    ]); ?>

</div>
<?php Pjax::end(); ?>
