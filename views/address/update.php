<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var nitm\geography\models\Drink $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: {name}', [
  'modelClass' => $model->properName(),
  'name' => $model->title
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drinks'), 'url' => [parse_url(\Yii::$app->request->getReferrer(), PHP_URL_PATH).parse_url(\Yii::$app->request->getReferrer(), PHP_URL_QUERY)]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drinks'), 'url' => ['/list/'.$model->isWhat()."/"]];

$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div id="<?= $formOptions['container']['id']; ?>" class="<?= $formOptions['container']['class'] ?>">
	<?php if(!\Yii::$app->request->isAjax): ?>
	<?= \yii\widgets\Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]); ?>
	<h2><?= Html::encode($this->title) ?></h2>
	<?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
		'formOptions' => $formOptions,
		'scenario' => $scenario,
		'action' => $action,
		'type' => $type
    ]) ?>
</div>
