<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var nitm\geography\models\BasicIngredient $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Drink',
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $model->properName()), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="<?= $formOptions['container']['id']; ?>" class="<?= $formOptions['container']['class']?> ">

	<?php if(!\Yii::$app->request->isAjax): ?>
		<?= \yii\widgets\Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]); ?>
		<h2><?= Html::encode($this->title) ?></h2>
	<?php endif; ?>

	<h4 class="alert alert-info">Create a drink. Once the basic information is created you can update it with instructions, images and more.</h4>
    <?= $this->render('_form', [
        'model' => $model,
		'formOptions' => $formOptions,
		'scenario' => $scenario,
		'action' => $action,
		'type' => $type
    ]) ?>
</div>
