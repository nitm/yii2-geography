<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/**
 * @var yii\web\View
 * @var nitm\geography\models\Address $model
 * @var yii\widgets\ActiveForm        $form
 */
?>

<div id="address-form-container" class="clearfix">
<?php
    $formOptions['type'] = ActiveForm::TYPE_VERTICAL;
    if (!isset($formOptions['action'])) {
        $formOptions['action'] = '/nitm-geography/address/save-address';
    }
    $form = include \Yii::getAlias('@nitm/views/layouts/form/header.php');
    if ($model->isNewRecord) {
        echo Html::tag('div', Html::tag('h3', 'There is no address!')
        .Html::tag('p', 'Please enter one below in the format: {NAME}, {STREET}, {CITY}, {STATE} {ZIPCODE}, {COUNTRY}.<br><br>i.e: Wac Arnolds, 2100-2198 E 32nd St, Cleveland, OH 44115, United States'), [
            'class' => 'alert alert-warning',
        ]);
    }
    echo $form->field($model, 'address', [
        'addon' => [
            'append' => [
                'asButton' => true,
                'content' => Html::submitButton(Icon::show('save').Html::tag('span', ' Save Address'), [
                    'class' => 'btn btn-info',
                    'type' => 'submit',
                ]),
            ],
        ],
    ])->textInput([
        'role' => 'mapAddress mapStreetAddress',
        'data-map-id' => 'map'.$model->getId(),
        'value' => $model->address,
        'title' => $model->title(),
        'placeholder' => 'Start typing to find an address',
    ])
    ->label('Address', ['class' => 'sr-only']);
?>
	<div class="hidden">
	<?php
      //   echo $form->field($model, 'address_id')->hiddenInput([
      //       'value' => $model->getId(),
      //   ])->label('Address', ['class' => 'sr-only']);
        echo $form->field($model, 'latitude')->hiddenInput([
            'role' => 'mapLatitude',
        ])->label('Latitude', ['class' => 'sr-only']);
        echo $form->field($model, 'longitude')->hiddenInput([
            'role' => 'mapLongitude',
        ])->label('Longitude', ['class' => 'sr-only']);
        echo $form->field($model, 'state_id')->hiddenInput([
            'role' => 'mapState',
        ])->label('State', ['class' => 'sr-only']);
        echo $form->field($model, 'city_id')->hiddenInput([
            'role' => 'mapCity',
        ])->label('City', ['class' => 'sr-only']);
        echo $form->field($model, 'country_id')->hiddenInput([
            'role' => 'mapCountry',
        ])->label('Country', ['class' => 'sr-only']);
        echo $form->field($model, 'zipcode')->hiddenInput([
            'role' => 'mapZipcode',
        ])->label('Zipcode', ['class' => 'sr-only']);
        echo $form->field($model, 'neighborhood_id')->hiddenInput([
            'role' => 'mapNeighborhood',
        ])->label('Neighborhood', ['class' => 'sr-only']);
    ?>
	</div>
<?php \kartik\widgets\ActiveForm::end(); ?>
	<div id="type-selector" class="controls">
	  <input type="radio" name="type" id="changetype-all" checked="checked">
	  <label for="changetype-all">All</label>

	  <input type="radio" name="type" id="changetype-establishment">
	  <label for="changetype-establishment">Establishments</label>

	  <input type="radio" name="type" id="changetype-address">
	  <label for="changetype-address">Addresses</label>

	  <input type="radio" name="type" id="changetype-geocode">
	  <label for="changetype-geocode">Geocodes</label>
	</div>
	<div id="map<?= $model->getId(); ?>" role="map" data-address='<?=json_encode($model->toArray())?>' style="height:500px">
	</div>
</div>
