<?php

use yii\helpers\Html;

return [
    'afterRow' => function ($model, $key, $index, $widget) {
        return [
            'items' => [
                [
                    'label' => 'Ambiance List '.Html::tag('span', $model->listMeta('ambiance')->count(true), [
                        'class' => 'badge',
                        'id' => 'ambiance-count-indicator'.$model->isWhat().$model->getId()
                    ]),
                    'encode' => false,
                    'content' =>  '',
                    'linkOptions' => [
                        'data-id' => '#ambiances-container'.$model->getId(),
                        'data-url' => \Yii::$app->urlManager->createUrl([
                            '/ambiance-list/index/'.$model->isWhat().'/'.$model->getId(),
                            '_format' => 'widget:json'
                        ]),
                    ]
                ], [
                    'label' => 'Mixologist List '.Html::tag('span', $model->listMeta('mixologist')->count(true), [
                        'class' => 'badge',
                        'id' => 'mixologist-count-indicator'.$model->isWhat().$model->getId()
                    ]),
                    'encode' => false,
                    'content' =>  '',
                    'linkOptions' => [
                        'data-id' => '#mixologists-container'.$model->getId(),
                        'data-url' => \Yii::$app->urlManager->createUrl([
                            '/mixologist-list/index/'.$model->isWhat().'/'.$model->getId(),
                            '_format' => 'widget:json'
                        ]),
                    ]
                ], [
                    'label' => 'Drink List '.Html::tag('span', $model->listMeta('drink')->count(true), [
                        'class' => 'badge',
                        'id' => 'drink-count-indicator'.$model->isWhat().$model->getId()
                    ]),
                    'encode' => false,
                    'content' =>  '',
                    'linkOptions' => [
                        'data-id' => '#drinks-container'.$model->getId(),
                        'data-url' => \Yii::$app->urlManager->createUrl([
                            '/drink-list/index/'.$model->isWhat().'/'.$model->getId(),
                            '_format' => 'widget:json'
                        ]),
                    ]
                ],
            ]
        ];
    }
];
?>
