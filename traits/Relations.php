<?php

namespace nitm\geography\traits;

use nitm\geography\models\Country;
use nitm\geography\models\State;

/**
 * Class Replies.
 */
trait Relations
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])
            ->orderBy(['name' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id'])
            ->orderBy(['name' => SORT_ASC]);
    }

    public function title()
    {
        return $this->title;
    }

    public static function primaryKey()
    {
        return ['id'];
    }
}
